# Trove Downloader

This program allows Humble Monthly subscribers to download all the games currently available in the Trove. Trove Downloader currently supports Windows and Linux. MacOS support is planned for the future. This program requires an active Humble Monthly subscription.

The Trove is a collection of 90+ DRM-Free games available to Humble Monthly subscribers. The collection include Humble original games alongside others, and are available to download only while subscribed to Humble Monthly. Games are added and removed every month, and there is no official way of tracking when games are removed from the Trove. This program aims to alleviate that.

*****
## Getting Started

An API key from Humble is required for this program to work. Instructions are provided in API_KEY.md on how to retrieve one.

To get started using Trove Downloader, either compile the program yourself, or download one of the binaries provided below.

The API key needs to be provided the first time the program is run. The key and all other parameters only need to be provided once. These parameters are saved and can be updated by giving a new flag on a future run. For example, running ``./trove_downloader -p linux`` will change the platform parameter to Linux, regardless of what it was before.

### Compiling

Make sure rust is installed on your system.
To compile trove_downloader, download the source files from this repo, cd into the source directory from a terminal, and run ``cargo build --release``. The compiled program will be located under ``./target/release/`` in the source directory.

### Installation

Pre-compiled binaries for Windows and Linux:
* [Linux](https://gitlab.com/silver_rust/trove_downloader/-/jobs/artifacts/master/raw/target/x86_64-unknown-linux-musl/release/trove_downloader?job=x86_64-unknown-linux-musl)
* [Windows](https://gitlab.com/silver_rust/trove_downloader/-/jobs/artifacts/master/raw/target/x86_64-pc-windows-gnu/release/trove_downloader.exe?job=x86_64-pc-windows-gnu)

### Usage

``trove_downloader <SUBCOMMAND> <FLAGS>``

FLAGS:
```
    -h, --help       Prints help information
    -V, --version    Prints version information
```
SUBCOMMANDS:
```
    help      Prints this message or the help of the given subcommand(s)
    run       Download Trove games given provided or saved flags
    update    Downloads the latest version of the program from this repo
```

``trove_downloader run <FLAGS>``

FLAGS:
```
    -f, --folders <folders>      Determines the folder layout. Options include:

        Original: Folder by platform then publisher
        Ex:     -f Original -> ./windows/Frozenbyte/trinegame_v1234.zip

        Game: Folder by platform then game name
        Ex      -f Game -> ./windows/Trine/trinegame_v1234.zip

        Reverse: Folder by game name then platform
        Ex:     -f Reverse -> ./Trine/windows/trinegame_v1234.zip

        Any combination of name, platform, and offset separated by commas will also suffice
        Ex:     -f name,platform -> ./Trine/windows/trinegame_v1234.zip

        [default: Game]

    -k, --key <key>              Session key. Optional after the first run.

        Ex:     -k "1234567890"   

    -l, --location <location>    Location where the games are saved. Defaults to the same folder as the executable. Optional after the first run.

        Ex:     -l "/home/user/games/trove"
                -l "C:\Games\Trove"

    -p, --platform <platform>    Possible values: all, windows, mac, linux, asmjs, unitywasm. Defaults to windows if left blank. Optional after the first run.

        Ex:     -p all
                -p windows,linux
```
## Example Uses

### First run

This command will download all games for every platform to the location ``C:\Games\Trove`` with the Reverse folder order if run on Windows.
```
.\trove_downloader.exe run -k "API KEY HERE" -p "all" -l "C:\Games\Trove\" -f Reverse
```

This command will download all games for every platform to the location ``~/games/trove`` with the Reverse folder order if run on Linux.
```
./trove_downloader run -k "API KEY HERE" -p "all" -l "~/games/trove" -f Reverse
```

### Future runs

This command will download all games given the saved parameters on Windows.
``.\trove_downloader.exe run``

This command will download all games given the saved parameters on Linux.
``./trove_downloader run``

*****

## To-Do List

* MacOS Support
* [add to do items here]

## License

This project is licensed under Blue Oak Model License version 1.0.0, as described in LICENSE.md.

## Contact

Discord: Silver#5563
Discord Server: https://discord.gg/4fQYyVb

Pull requests and ideas welcome.



This program is not affiliated nor endorsed by Humble Bundle, Inc.
